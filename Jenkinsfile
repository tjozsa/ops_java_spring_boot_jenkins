pipeline {
    agent any
    stages {
        stage("Clean workspace") {
            steps {
                deleteDir()
            }
        }
        stage("Env Variables") {
            agent any
            steps {
                echo "${env}"
                echo "The build number is ${env.BUILD_NUMBER}"
                echo "You can also use \${BUILD_NUMBER} -> ${BUILD_NUMBER}"
            }
        }
        stage('Clone') {
            agent any
            steps {
                git credentialsId: 'devops_jenkins', url: "${env.GIT_URL}"
            }
        }
        stage('Build') {
            agent {
                docker {
                    image 'maven:3-alpine'
                    args '-v $HOME/.m2:/root/.m2'
                }
            }
            steps {
                sh 'mvn clean compile'
            }
        }
        stage('Unit test') {
            agent {
                docker {
                    image 'maven:3-alpine'
                    args '-v $HOME/.m2:/root/.m2'
                }
            }
            steps {
                sh 'mvn test'
                stash(name: "testresults", includes: '**/target/surefire-reports/*.xml')
            }
        }
        stage('Archive') {
            agent {
                docker {
                    image 'maven:3-alpine'
                    args '-v $HOME/.m2:/root/.m2'
                }
            }
            steps {
                sh 'mvn package -Dmaven.test.skip=true'
                stash(name: "jars", includes: '**/target/*.jar')
            }
        }
        stage('Code quality check') {
            agent {
                docker {
                    image 'maven:3-alpine'
                    args '-v $HOME/.m2:/root/.m2'
                }
            }
            steps {
                withSonarQubeEnv('sonar') { 
                    sh 'mvn sonar:sonar'
                }
            }
        }
       stage("Quality Gate") {
            steps {
                timeout(time: 1, unit: 'HOURS') {
                    waitForQualityGate abortPipeline: true
                }
            }
        }
        stage('Prepare docker image') {
            agent any
            steps {
                script {
                    def fileName = "serving-web-content-0.0.1-SNAPSHOT.jar" 
                    repository =  "devopsacademy.jfrog.io/docker/serving-web-content"            
                    tag = "$BUILD_NUMBER-dev" 
                    repo_with_tag = repository + ":" + tag 
                    rtdockerImage = docker.build repo_with_tag, "--build-arg JAR_NAME=${fileName} ."
                } 
            }
        }
        stage('Package deploy') {
            agent {
                docker {
                    image 'maven:3-alpine'
                    args '-v $HOME/.m2:/root/.m2 --network devops_ci_cd_cicd-demo-network'
                }
            }
            steps {
                sh 'mvn --settings settings.xml deploy'
            }
        }
        stage('Artifactory docker push') {
            steps {
                script {
                    docker.withRegistry('https://devopsacademy.jfrog.io/docker', 'artifactory_credential') {
                        /* Push the container to the custom Registry */         
                        rtdockerImage.push()
                    }
                }
            }
        } 
    }
    post {
        success {
            unstash("jars")
            archiveArtifacts artifacts: '**/*.jar', fingerprint: true
            unstash("testresults")
            junit '**/*.xml'
        }
    }
}
